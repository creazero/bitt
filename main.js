'use strict';

(function() {
  const MODULE_CLASS = 'module';
  const MODULE_BTN_CLASS = 'module__connect';

  let drawerEl;
  let svgDrawer;
  let activeLine;
  
  init();

  function addModule() {
    const selectValue = getSelectValue();
    const type = !selectValue ? 'in' : selectValue;

    const moduleEl = document.createElement('div');
    moduleEl.className = [MODULE_CLASS, `${MODULE_CLASS}__${type}`].join(' ');
    moduleEl.style.top = `${drawerEl.clientHeight / 2}px`;
    moduleEl.style.left = `${drawerEl.clientWidth / 2}px`;

    if (type === 'in_out') {
      const inBtn = createConnectionBtn('in');
      moduleEl.appendChild(inBtn)

      const outBtn = createConnectionBtn('out');
      moduleEl.appendChild(outBtn)
    } else {
      const connectBtn = createConnectionBtn(type);
      moduleEl.appendChild(connectBtn);
    }
    
    drawerEl.appendChild(moduleEl);
  }

  function attachInEvents(btnEl) {
    btnEl.addEventListener('click', () => {
      if (activeLine) {
        window.removeEventListener('mousemove', onMouseMove);
        activeLine = null;
      }
    });
  }

  function attachOutEvents(btnEl) {
    btnEl.addEventListener('click', event => {
      activeLine = document.createElementNS('http://www.w3.org/2000/svg','line');

      const drawerRect = drawerEl.getBoundingClientRect();
      const btnRect = event.currentTarget.getBoundingClientRect();
      activeLine.setAttribute('x1', btnRect.x - drawerRect.left + 15);
      activeLine.setAttribute('y1', btnRect.y - drawerRect.top + 15);
      activeLine.setAttribute('stroke', 'black');
      activeLine.setAttribute('stroke-width', '3px');

      svgDrawer.appendChild(activeLine);

      window.addEventListener('mousemove', onMouseMove);

      const onKeyDown = event => {
        if (event.key === 'Escape' && activeLine) {
          window.removeEventListener('mousemove', onMouseMove);
          svgDrawer.removeChild(activeLine);
          activeLine = null;
          window.removeEventListener('keydown', onKeyDown);
        }
      };
      window.addEventListener('keydown', onKeyDown);
    });
  }

  function createConnectionBtn(type) {
    if (type !== 'in' && type !== 'out') return;

    const btnEl = document.createElement('button');
    btnEl.className = [MODULE_BTN_CLASS, `${MODULE_BTN_CLASS}__${type}`].join(' ');

    const el = document.createElement('i');
    el.className = 'material-icons';
    el.textContent = type === 'in' ? 'arrow_right' : 'add';

    btnEl.appendChild(el);

    if (type === 'out') {
      attachOutEvents(btnEl);
    } else {
      attachInEvents(btnEl);
    }

    return btnEl;
  }

  function getSelectValue() {
    const selectElement = document.getElementById('moduleType');

    if (!selectElement) return null;

    return selectElement.options[selectElement.selectedIndex].value;
  }

  function init() {
    initEventListeners();

    drawerEl = document.getElementById('drawer');
    svgDrawer = document.getElementById('svgDrawer');

    const draggable = new Draggable.default([drawerEl], { draggable: '.module' })

    let position;

    draggable.on('drag:move', event => {
      const rect = svgDrawer.getBoundingClientRect();
      position = {
        clientX: event.originalEvent.clientX - rect.left,
        clientY: event.originalEvent.clientY - rect.top
      }
    });

    draggable.on('drag:stop', ({ originalSource }) => {
      originalSource.style.top = `${position.clientY}px`;
      originalSource.style.left = `${position.clientX}px`;
      position = null;
    });
  }

  function initEventListeners() {
    const addButton = document.getElementById('addBtn');

    addButton.addEventListener('click', addModule);
  }

  function onMouseMove(event) {
    const rect = drawerEl.getBoundingClientRect();
    activeLine.setAttribute('x2', event.clientX - rect.left);
    activeLine.setAttribute('y2', event.clientY - rect.top);
  }
})();